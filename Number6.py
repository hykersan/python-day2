
def combine_values(n1, n2):
    if type(n1) == str and type(n2) == str:
        return n1 + n2
    elif type(n1) == int and type(n2) == int:
        return n1 * n2
    elif type(n1) == list and type(n2) == list:
        return n1 + n2
    elif type(n1) == dict and type(n2) == dict:
        return {**n1, **n2}

    else:
        return "Values are of different types or not supported"



# two strings
print(combine_values("Hyker", " Murillo"))

# two integers
print(combine_values(10,34))

# two lists
print(combine_values([1, 2, 3], [4, 5, 6]))

# two dictionaries
print(combine_values({'a': 1, 'b': 2}, {'c': 3, 'd': 4}))



