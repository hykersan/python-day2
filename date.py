

#
from datetime import datetime
from datetime import timedelta

now = datetime.now()
str_dt1 = datetime.now()


date_time = now.strftime("%b/%d/%Y, %H:%M:%S")
print("date and time:",date_time)

specific_timedelta = timedelta(days=3, hours=2, minutes=15, seconds=30)


three_days = (str_dt1 + specific_timedelta).strftime("%B %d, %Y, %H:%M:%S")

print(f"The Date and time in 3 days, 2 hour, and 15 minutes will be {three_days}.")